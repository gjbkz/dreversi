#ifndef DREVERSI_LIB_CLOCK
#define DREVERSI_LIB_CLOCK

#include <stdint.h>

#define N_MAX_CLOCK_COUNT 0xFFFF
/** @brief UART1ビット間のTIM2割り込み発生回数です。 */
#define N_TIM2_PER_BIT 16
typedef uint32_t clock_t;

clock_t tick(void);
clock_t getTime(clock_t from);

#endif  // DREVERSI_LIB_CLOCK
