#include "LSB2MSB.hpp"

int calculatePos(clock_t startedAt) {
  clock_t elapsed = getTime(startedAt);
  if (N_MAX_ELAPSED <= elapsed) {
    return N_MAX_POS;
  }
  return elapsed / N_TIM2_PER_BIT;
}

bool getBitAt(uint8_t byte, uint8_t pos) {
  if (1 <= pos && pos <= 8) {
    return (byte >> (pos - 1)) & 0x01;
  }
  return pos != 0;
}

uint8_t setBitAt(uint8_t byte, uint8_t pos, bool bit) {
  if (1 <= pos && pos <= 8) {
    if (bit) {
      return byte | (0x01 << (pos - 1));
    } else {
      return byte & ~(0x01 << (pos - 1));
    }
  }
  return byte;
}

LSB2MSB::LSB2MSB(uint8_t offset)
    : byte(0), startedAt(0), offset(offset), pos(N_MAX_POS) {}

void LSB2MSB::setByte(uint8_t byte) {
  this->byte = byte;
  this->startedAt = getTime(0) - this->offset;
  this->pos = -1;
}

bool LSB2MSB::isDone() {
  return N_MAX_POS <= this->pos;
}

uint8_t LSB2MSB::getByte() {
  return this->byte;
}

int LSB2MSB::getPos() {
  return this->pos;
}

bool LSB2MSB::syncPos() {
  int pos = calculatePos(this->startedAt);
  if (this->pos < pos) {
    this->pos = pos;
    return true;
  }
  return false;
}

bool LSB2MSB::getCurrentBit() {
  return getBitAt(this->byte, this->pos);
}

void LSB2MSB::setCurrentBit(bool bit) {
  this->byte = setBitAt(this->byte, this->pos, bit);
}
