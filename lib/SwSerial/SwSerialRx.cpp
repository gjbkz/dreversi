#include "SwSerialRx.hpp"
#include <stdio.h>

void SwSerialRx::free() {
  if (this->EXTI_Line != EXTI_Line_Null) {
    this->EXTI_Line = EXTI_Line_Null;
  }
}

SwSerialRx::SwSerialRx(GPIO_TypeDef* GPIOx, u16 pin)
    : config(new EXTIConfig(GPIOx, pin)),
      buffer(new RingBuffer(N_RXBUF)),
      data(new LSB2MSB()),
      EXTI_Line(EXTI_Line_Null) {}

bool SwSerialRx::configure() {
  GPIO_InitTypeDef ini = {0};
  ini.GPIO_Speed = GPIO_Speed_50MHz;
  ini.GPIO_Pin = this->config->GPIO_Pin;
  ini.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_Init(this->config->GPIOx, &ini);

  /** スタートビット検出のための割り込み設定 */
  GPIO_EXTILineConfig(this->config->GPIO_PortSource,
                      this->config->GPIO_PinSource);
  EXTI_InitTypeDef EXTI_InitStructure = {0};
  EXTI_InitStructure.EXTI_Line = this->config->EXTI_Line;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  NVIC_InitTypeDef NVIC_InitStructure = {0};
  NVIC_InitStructure.NVIC_IRQChannel = EXTI7_0_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 5;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  return false;
}

bool SwSerialRx::begin() {
  if (this->configure()) {
    return true;
  }
  return false;
}

bool SwSerialRx::hasData() {
  return 0 < this->buffer->length();
}

bool SwSerialRx::isReading() {
  return this->EXTI_Line != EXTI_Line_Null;
}

void SwSerialRx::trigger(uint32_t EXTI_Line) {
  if (this->isReading()) {
    return;
  }
  this->EXTI_Line = EXTI_Line;
  this->data->setByte(0);
}

bool SwSerialRx::readState() {
  return GPIO_ReadInputDataBit(this->config->GPIOx, this->config->GPIO_Pin);
}

void SwSerialRx::run() {
  if (!this->isReading()) {
    return;
  }
  if (this->data->syncPos()) {
    int pos = this->data->getPos();
    if (pos == 0) {
      // スタートビット中のため何もしない
    } else if (1 <= pos && pos <= 8) {
      this->data->setCurrentBit(this->readState());
    } else if (pos == N_POS_STOP_BIT) {
      this->buffer->push(this->data->getByte());
    } else {
      this->free();
    }
  }
}

uint8_t SwSerialRx::read() {
  return this->buffer->shift();
}
