#ifndef DREVERSI_LIB_RINGBUFFER
#define DREVERSI_LIB_RINGBUFFER

#include <stdint.h>

class RingBuffer {
 private:
  uint16_t size;
  uint16_t head;
  uint16_t tail;
  uint8_t* buffer;

 public:
  RingBuffer(uint16_t size);
  uint16_t length();
  void push(uint8_t data);
  uint8_t shift(void);
};

#endif  // DREVERSI_LIB_RINGBUFFER
