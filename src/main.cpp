#include <Button.hpp>
#include <Clock.hpp>
#include <LED.hpp>
#include <SwSerialRx.hpp>
#include <SwSerialTx.hpp>
#include <App.hpp>
#include "debug.h"

#define N_UART_CH 4

SwSerialTx *tx[N_UART_CH] = {
    new SwSerialTx(GPIOC, GPIO_Pin_2), // E
    new SwSerialTx(GPIOC, GPIO_Pin_4), // N
    new SwSerialTx(GPIOC, GPIO_Pin_6), // W
    new SwSerialTx(GPIOD, GPIO_Pin_3), // S
};

SwSerialRx *rx[N_UART_CH] = {
    new SwSerialRx(GPIOC, GPIO_Pin_1), // E
    new SwSerialRx(GPIOC, GPIO_Pin_3), // N
    new SwSerialRx(GPIOC, GPIO_Pin_5), // W
    new SwSerialRx(GPIOD, GPIO_Pin_2), // S
};

Button *button = new Button(GPIOC, GPIO_Pin_0, 15000);
LED *led = new LED(GPIOD, GPIO_Pin_0, GPIOD, GPIO_Pin_4);

void TIM2_Init(u16 arr, u16 psc)
{
  TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;

  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
  TIM_TimeBaseInitStructure.TIM_Period = arr;
  TIM_TimeBaseInitStructure.TIM_Prescaler = psc;
  TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStructure);

  TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

  NVIC_InitTypeDef NVIC_InitStructure = {0};
  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
  /** 最優先でないとデバッグのUARTでタイマー割り込みが止まる */
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 5;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  TIM_ARRPreloadConfig(TIM2, ENABLE);
  TIM_Cmd(TIM2, ENABLE);
}

int main(void)
{
  Delay_Init();
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
  RCC_APB2PeriphClockCmd(
      RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD,
      ENABLE);
  USART_Printf_Init(115200);
  Delay_Ms(300);
  led->init();
  led->set(0b01);
  button->init();
  TIM2_Init(30, 9);
  led->set(0b10);
  for (u8 i = 0; i < 4; i++)
  {
    tx[i]->begin();
    rx[i]->begin();
  }
  led->blink(40, 1, 2);
  while (1)
  {
    for (u8 i = 0; i < N_UART_CH; i++)
    {
      if (rx[i]->hasData())
      {
        while (rx[i]->hasData())
        {
          app.onReceive(i, rx[i]->read());
        }
      }
    }
    if (button->isPressed())
    {
      button->reset();
      app.onPress(i, rx[i]->read());
    }
  }
}

void handleTx(uint16_t count)
{
  static uint8_t ch = 0;
  tx[ch]->run();
  ch = (ch + 1) % N_UART_CH;
}

void handleRx(uint16_t _count)
{
  static uint8_t ch = 0;
  rx[ch]->run();
  ch = (ch + 1) % N_UART_CH;
}

extern "C" void TIM2_IRQHandler(void)
    __attribute__((interrupt("WCH-Interrupt-fast")));
void TIM2_IRQHandler(void)
{
  TIM_ClearFlag(TIM2, TIM_FLAG_Update);
  uint16_t count = tick();
  if (count % 2)
  {
    handleTx(count);
  }
  else
  {
    handleRx(count);
  }
}

extern "C" void EXTI7_0_IRQHandler(void)
    __attribute__((interrupt("WCH-Interrupt-fast")));
void EXTI7_0_IRQHandler(void)
{
  if (EXTI_GetITStatus(EXTI_Line1) != RESET)
  {
    EXTI_ClearITPendingBit(EXTI_Line1);
    rx[0]->trigger(EXTI_Line1);
  }
  if (EXTI_GetITStatus(EXTI_Line3) != RESET)
  {
    EXTI_ClearITPendingBit(EXTI_Line3);
    rx[1]->trigger(EXTI_Line3);
  }
  if (EXTI_GetITStatus(EXTI_Line5) != RESET)
  {
    EXTI_ClearITPendingBit(EXTI_Line5);
    rx[2]->trigger(EXTI_Line5);
  }
  if (EXTI_GetITStatus(EXTI_Line2) != RESET)
  {
    EXTI_ClearITPendingBit(EXTI_Line2);
    rx[3]->trigger(EXTI_Line2);
  }
  if (EXTI_GetITStatus(EXTI_Line0) != RESET)
  {
    EXTI_ClearITPendingBit(EXTI_Line0);
    button->onPress();
  }
}
