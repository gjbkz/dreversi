#ifndef DREVERSI_LIB_LED
#define DREVERSI_LIB_LED

#include <ch32v00x.h>

class LED {
 private:
  GPIO_TypeDef* GPIOx1;
  u16 pin1;
  GPIO_TypeDef* GPIOx2;
  u16 pin2;

 public:
  LED(GPIO_TypeDef* GPIOx1, u16 pin1, GPIO_TypeDef* GPIOx2, u16 pin2);
  /**
   * LEDの点灯状態です。LSBが青、その次が赤です。
   * @example
   * 0b11: 赤点灯 青点灯
   * 0b01: 赤点灯 青消灯
   * 0b10: 赤消灯 青点灯
   * 0b00: 赤消灯 青消灯
   */
  uint8_t state;
  /** LEDのGPIOを設定します */
  void init();
  void set(uint8_t state);
  /** LEDを交互に点滅させます */
  void blink(
      /** 点滅の時間間隔 */
      uint8_t interval,
      /**
       * 点滅モード
       * 0: 01 → 10 → 01 → 10 → ...
       * 1: 11 → 00 → 11 → 00 → ...
       * 2: 00 → 01 → 11 → 10 → 00 → ...
       */
      uint8_t mode,
      /** 点滅回数 */
      uint8_t n);
};

#endif  // DREVERSI_LIB_LED
