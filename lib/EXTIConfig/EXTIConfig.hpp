#ifndef DREVERSI_LIB_EXTICONFIG
#define DREVERSI_LIB_EXTICONFIG

#include <ch32v00x.h>

class EXTIConfig {
 public:
  bool error = false;
  GPIO_TypeDef* GPIOx;
  u16 GPIO_Pin;
  u8 GPIO_PortSource;
  u8 GPIO_PinSource;
  u32 EXTI_Line;

  EXTIConfig(GPIO_TypeDef* GPIOx, u16 GPIO_Pin);
};

#endif  // DREVERSI_LIB_EXTICONFIG
