#include "SwSerialTx.hpp"

SwSerialTx::SwSerialTx(GPIO_TypeDef* GPIOx, u16 pin)
    : config(new EXTIConfig(GPIOx, pin)),
      buffer(new RingBuffer(N_TXBUF)),
      data(new LSB2MSB()) {}

bool SwSerialTx::configure() {
  GPIO_InitTypeDef ini = {0};
  ini.GPIO_Speed = GPIO_Speed_50MHz;
  ini.GPIO_Pin = this->config->GPIO_Pin;
  ini.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(this->config->GPIOx, &ini);
  return false;
}

bool SwSerialTx::begin() {
  if (this->configure()) {
    return true;
  }
  this->setState(true);
  return false;
}

bool SwSerialTx::hasData() {
  return 0 < this->buffer->length();
}

void SwSerialTx::run() {
  /** 現在のbyteが送信済みで次のbyteがあるか？ */
  if (this->data->isDone()) {
    if (this->hasData()) {
      /** 次のbyteをセットする */
      this->data->setByte(this->buffer->shift());
    } else {
      return;
    }
  }
  if (this->data->syncPos()) {
    /** ビットの状態を更新する */
    this->setState(this->data->getCurrentBit());
  }
}

void SwSerialTx::setState(bool state) {
  this->state = state;
  GPIO_WriteBit(this->config->GPIOx, this->config->GPIO_Pin,
                state ? Bit_SET : Bit_RESET);
}

void SwSerialTx::push(char c) {
  this->buffer->push(c);
}
