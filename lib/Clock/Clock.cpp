#include "Clock.hpp"

clock_t count = 0;

clock_t tick(void) {
  return ++count;
}

clock_t getTime(clock_t from) {
  if (count < from) {
    return (N_MAX_CLOCK_COUNT - from) + count;
  }
  return count - from;
}
