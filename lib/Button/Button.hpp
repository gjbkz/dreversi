#ifndef DREVERSI_LIB_Button
#define DREVERSI_LIB_Button

#include <ch32v00x.h>
#include <Clock.hpp>

class Button {
 private:
  GPIO_TypeDef* GPIOx;
  u16 pin;
  /**
   * ボタンの状態
   * 0: 待機中
   * 1: 割り込みがあった
   * 2: チャタリング防止のためのデッドタイム中
   */
  uint8_t state;
  clock_t lastPressedAt;
  clock_t deadTime;

 public:
  Button(GPIO_TypeDef* GPIOx, u16 pin, clock_t deadTime);
  /** ButtonのGPIOを設定します */
  void init();
  /** 押されたときに呼びます */
  void onPress();
  /** 押されていればtrueを返します */
  bool isPressed();
  /** 押された状態を解除します */
  void reset();
};

#endif  // DREVERSI_LIB_Button
