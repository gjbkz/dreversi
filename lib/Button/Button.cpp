#include "Button.hpp"
#include <ch32v00x.h>
#include <Clock.hpp>
#include <EXTIConfig.hpp>

Button::Button(GPIO_TypeDef* GPIOx, u16 pin, clock_t deadTime)
    : GPIOx(GPIOx), pin(pin), deadTime(deadTime) {
  this->state = 0;
  this->lastPressedAt = 0;
}

void Button::init() {
  EXTIConfig* config = new EXTIConfig(this->GPIOx, this->pin);
  GPIO_InitTypeDef GPIO_InitStructure = {0};

  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Pin = config->GPIO_Pin;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
  GPIO_Init(config->GPIOx, &GPIO_InitStructure);

  EXTI_InitTypeDef EXTI_InitStructure = {0};
  GPIO_EXTILineConfig(config->GPIO_PortSource, config->GPIO_PinSource);
  EXTI_InitStructure.EXTI_Line = config->EXTI_Line;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  NVIC_InitTypeDef NVIC_InitStructure = {0};
  NVIC_InitStructure.NVIC_IRQChannel = EXTI7_0_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 5;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

void Button::onPress() {
  if (this->state == 0) {
    this->state = 1;
    this->lastPressedAt = getTime(0);
  }
}

bool Button::isPressed() {
  if (this->state == 2) {
    if (this->deadTime < getTime(this->lastPressedAt)) {
      this->state = 0;
    }
    return false;
  }
  return this->state == 1;
}

void Button::reset() {
  if (this->state == 0) {
    return;
  }
  if (this->deadTime < getTime(this->lastPressedAt)) {
    this->state = 0;
  } else {
    this->state = 2;
  }
}
