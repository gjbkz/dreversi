#ifndef DREVERSI_LIB_LSB2MSB
#define DREVERSI_LIB_LSB2MSB

#include <Clock.hpp>

#define N_POS_STOP_BIT 9
#define N_MAX_POS 10
#define N_MAX_ELAPSED (N_MAX_POS * N_TIM2_PER_BIT)

/**
 * @brief byteの左からposビット目を返します。posは1から8までの値を取ります。
 */
bool getBitAt(uint8_t byte, uint8_t pos);

class LSB2MSB {
 private:
  uint8_t byte;
  clock_t startedAt;
  clock_t offset;
  /**
   *   0: スタートビット送信中
   * 1-8: データビット送信中
   *   9: ストップビット送信中
   *  10: 送信完了
   */
  int pos;

 public:
  uint8_t getByte();
  int getPos();
  LSB2MSB(uint8_t offset = 0);
  /** @brief byteをセットします。 */
  void setByte(uint8_t byte);
  bool isDone();
  /** @brief posの値を更新します。更新された場合はtrueを返します。 */
  bool syncPos();
  /** @returns 現在のビットを返します。 */
  bool getCurrentBit();
  /** @returns 現在のビットをセットします。 */
  void setCurrentBit(bool bit);
};

#endif  // DREVERSI_LIB_LSB2MSB
