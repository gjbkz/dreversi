#ifndef DREVERSI_LIB_SWSERIALTX
#define DREVERSI_LIB_SWSERIALTX

#include <EXTIConfig.hpp>
#include <RingBuffer.hpp>
#include "LSB2MSB.hpp"

#define N_TXBUF 16

class SwSerialTx {
 private:
  const EXTIConfig* config;
  RingBuffer* buffer;
  LSB2MSB* data;
  bool state;

 public:
  SwSerialTx(GPIO_TypeDef* GPIOx, u16 pin);
  bool configure();
  bool begin();
  bool hasData();
  void run();
  /**
   * ピンの状態を変更します。trueでHIGH、falseでLOWにします。
   */
  void setState(bool state);
  /**
   * データを送信バッファに追加します
   */
  void push(char c);
};

#endif  // DREVERSI_LIB_SWSERIALTX
