#include "LED.hpp"
#include <ch32v00x.h>
#include "debug.h"

LED::LED(GPIO_TypeDef* GPIOx1, u16 pin1, GPIO_TypeDef* GPIOx2, u16 pin2)
    : GPIOx1(GPIOx1), pin1(pin1), GPIOx2(GPIOx2), pin2(pin2) {
  this->state = 0;
}

void LED::init() {
  GPIO_InitTypeDef GPIO_InitStructure = {0};
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Pin = pin1;
  GPIO_Init(GPIOx1, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin = pin2;
  GPIO_Init(GPIOx2, &GPIO_InitStructure);
}

void LED::set(uint8_t state) {
  this->state = state;
  GPIO_WriteBit(GPIOx1, pin1, (state & 0b01) ? Bit_SET : Bit_RESET);
  GPIO_WriteBit(GPIOx2, pin2, (state & 0b10) ? Bit_SET : Bit_RESET);
}

void LED::blink(uint8_t interval, uint8_t mode, uint8_t n) {
  if (mode == 0) {
    for (u8 i = 0; i < n; i++) {
      set(0b01);
      Delay_Ms(interval);
      set(0b10);
      Delay_Ms(interval);
    }
  } else if (mode == 1) {
    for (u8 i = 0; i < n; i++) {
      set(0b11);
      Delay_Ms(interval);
      set(0b00);
      Delay_Ms(interval);
    }
  } else if (mode == 2) {
    for (u8 i = 0; i < n; i++) {
      set(0b01);
      Delay_Ms(interval);
      set(0b11);
      Delay_Ms(interval);
      set(0b10);
      Delay_Ms(interval);
      set(0b00);
      Delay_Ms(interval);
    }
  }
  Delay_Ms(interval);
  set(0b00);
}
