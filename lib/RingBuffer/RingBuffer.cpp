#include "RingBuffer.hpp"
#include <stdint.h>

RingBuffer::RingBuffer(uint16_t size)
    : size(size), head(0), tail(0), buffer(new uint8_t[size]) {}

uint16_t RingBuffer::length() {
  return (this->size + this->tail - this->head) % this->size;
}

void RingBuffer::push(uint8_t data) {
  this->buffer[this->tail] = data;
  this->tail = (this->tail + 1) % this->size;
  if (this->head == this->tail) {
    this->head = (this->head + 1) % this->size;
  }
}

uint8_t RingBuffer::shift(void) {
  uint8_t data = this->buffer[this->head];
  if (this->head != this->tail) {
    this->head = (this->head + 1) % this->size;
  }
  return data;
}
