#include "EXTIConfig.hpp"

EXTIConfig::EXTIConfig(GPIO_TypeDef* GPIOx, u16 GPIO_Pin)
    : GPIOx(GPIOx), GPIO_Pin(GPIO_Pin) {
  if (GPIOx == GPIOC) {
    this->GPIO_PortSource = GPIO_PortSourceGPIOC;
  } else if (GPIOx == GPIOD) {
    this->GPIO_PortSource = GPIO_PortSourceGPIOD;
  } else if (GPIOx == GPIOA) {
    this->GPIO_PortSource = GPIO_PortSourceGPIOA;
  } else {
    this->error = true;
    return;
  }
  switch (GPIO_Pin) {
    case GPIO_Pin_0:
      this->GPIO_PinSource = GPIO_PinSource0;
      this->EXTI_Line = EXTI_Line0;
      break;
    case GPIO_Pin_1:
      this->GPIO_PinSource = GPIO_PinSource1;
      this->EXTI_Line = EXTI_Line1;
      break;
    case GPIO_Pin_2:
      this->GPIO_PinSource = GPIO_PinSource2;
      this->EXTI_Line = EXTI_Line2;
      break;
    case GPIO_Pin_3:
      this->GPIO_PinSource = GPIO_PinSource3;
      this->EXTI_Line = EXTI_Line3;
      break;
    case GPIO_Pin_4:
      this->GPIO_PinSource = GPIO_PinSource4;
      this->EXTI_Line = EXTI_Line4;
      break;
    case GPIO_Pin_5:
      this->GPIO_PinSource = GPIO_PinSource5;
      this->EXTI_Line = EXTI_Line5;
      break;
    case GPIO_Pin_6:
      this->GPIO_PinSource = GPIO_PinSource6;
      this->EXTI_Line = EXTI_Line6;
      break;
    case GPIO_Pin_7:
      this->GPIO_PinSource = GPIO_PinSource7;
      this->EXTI_Line = EXTI_Line7;
      break;
    default:
      this->error = true;
      return;
  }
};
