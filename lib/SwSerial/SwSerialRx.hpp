#ifndef DREVERSI_LIB_SWSERIALRX
#define DREVERSI_LIB_SWSERIALRX

#include <EXTIConfig.hpp>
#include <RingBuffer.hpp>
#include "LSB2MSB.hpp"

#define N_RXBUF 16
#define EXTI_Line_Null 0

class SwSerialRx {
 private:
  const EXTIConfig* config;
  RingBuffer* buffer;
  LSB2MSB* data;
  uint32_t EXTI_Line;
  void free();

 public:
  SwSerialRx(GPIO_TypeDef* GPIOx, u16 pin);
  bool configure();
  bool begin();
  bool hasData();
  void run();
  bool readState();
  bool isReading();
  /** @brief スタートビットを検出したときに呼びます */
  void trigger(uint32_t EXTI_Line);
  uint8_t read();
};

#endif  // DREVERSI_LIB_SWSERIALRX
